import { callApi } from '../helpers/apiHelper';

class Fighter {
    constructor(name = 'Ryu', health = 1000, attack = 100, defense = 100, rating = 100) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.rating = rating;
    } 
  
    getHitPower() {
        return (this.attack*(Math.random()+1)).toFixed(0);
    }
  
    getBlockPower() {
        return (this.defense*(Math.random()+1)).toFixed(0);
    } 
  } 

export const fighter = new Fighter("Oleksii");