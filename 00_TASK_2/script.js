// FUNCTION superSort

function superSort(names) {
  let namesSorted = [];
  let namesOriginal = names.toLowerCase().split(" ");
  let namesNoDigits = names
    .toLowerCase()
    .replace(/[0-9]/g, "")
    .split(" ")
    .map((element, index) => `${element}${index.toString()}`)
    .sort();

  for (let i = 0; i < namesNoDigits.length; i++) {
    namesSorted[i] =
      namesOriginal[namesNoDigits[i][namesNoDigits[i].length - 1]];
  }
  return namesSorted;
}

// FUNCTION compareDates

function compareDates(dateInput1, dateInput2) {
  let date = [];
  const dateInput = [dateInput1, dateInput2];
  const separator = [/\./, /\,/, /\//, /\-/];
  let actualSeparator;
  let result;

  for (let j = 0; j < dateInput.length; j++) {
    let year;
    let month;
    let day;

    for (let i = 0; i < separator.length; i++) {
      actualSeparator = separator[i];
      if (dateInput[j].match(separator[i]) !== null) {
        break;
      }
    }

    date[j] = dateInput[j].split(actualSeparator);

    for (let i = 0; i < date[j].length; i++) {
      if (date[j][i].length === 4) {
          year = date[j][i];
      }
      if (date[j][i].length === 2 && parseInt(date[j][i]) > 12) {
        day = date[j][i];
      }
      if (date[j][i].length === 2 && parseInt(date[j][i]) <= 12) {
        month = date[j][i];
      }
      if (
        date[j][i].length === 2 &&
        parseInt(date[j][i]) <= 12 &&
        day === undefined
      ) {
        day = date[j][i];
      }
    }

    date[j] = [day, month, year];
  }

  for (let i = 0; i < date[1].length; i++) {
    if (date[0][i] !== date[1][i]) {
      result = false;
      break;
    } else {
      result = true;
    }
  }

  if (result) {
    console.log("Dates are equal");
  } else {
    console.log("Dates are NOT equal");
  }
}

// FUNCTION findUniqe

function findUniqe(array) {
  for (let i = 1; i < array.length - 1; i++) {
    if (array[i] !== array[i + 1] && array[i] !== array[i - 1]) {
      return array[i];
    }
  }
}

// FUNCTION getDiscount

function getDiscount(number = 0, priceOriginal = 0) {
  if (number >= 1 && number < 5 || number === 0) {
    return priceOriginal;
  }
  if (number >= 5 && number < 10) {
    return (priceOriginal * 0.95).toFixed(2);
  }
  if (number >= 10) {
    return (priceOriginal * 0.9).toFixed(2);
  }
}

// FUNCTION getDaysForMonth

function getDaysForMonth(month = 0) {
  switch (month) {
    case 2:
      return 28;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return 31;
    case 4:
    case 6:
    case 9:
    case 11:
      return 30;
    default:
      return 0;
  }
}

// console.log(superSort("mic09ha1el 4b5en6 michele be4atr3ice"));

// compareDates('26,05,2010', '05/06/2010');
// compareDates('26-05-2010', '26.2010.05');

// console.log(findUniqe([1, 1, 1, 2, 1, 1]));
// console.log(findUniqe([0, 0, 0, 55, 0, 0]));

// console.log(getDiscount(0, 346));
// console.log(getDiscount(2, 346));
// console.log(getDiscount(5, 346));
// console.log(getDiscount(7, 346));
// console.log(getDiscount(10, 346));
// console.log(getDiscount(5));
// console.log(getDiscount());


// console.log(getDaysForMonth(10));
// console.log(getDaysForMonth(6));
// console.log(getDaysForMonth(2));
// console.log(getDaysForMonth());